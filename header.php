<?php include('config.php');

if($site_dir == '') {
    $custdir = '';
} elseif ($site_dir != '') 
{ 
    $custdir = '/'.$site_dir;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $site_description; ?>">
    <meta name="author" content="CoreCms">

    <title><?php echo $site_name; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $custdir; ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $custdir; ?>/assets/fontawesome/css/all.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo $custdir; ?>/css/<?php echo $site_theme; ?>.css">

    <!-- Google reCaptcha -->
    <!-- not needed now <script src='https://www.google.com/recaptcha/api.js' async defer></script> -->
    <!-- Wowhead Tooltips -->
    <script>var whTooltips = {colorLinks: true, iconizeLinks: true, renameLinks: true};</script>
    <script src="https://wow.zamimg.com/widgets/power.js"></script>
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top nav-wow" id="main-nav">
    <div class="container">
        <a class="navbar-brand" href="<?php echo $custdir ?>">
            <i class="icon wow ">World of Warcraft</i>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php if($site_dir == '') { echo '/';} elseif ($site_dir != '') { echo '/'.$site_dir.'/';}  ?>"><i class="fad fa-home"></i> Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php if($site_dir == '') { echo '/';} elseif ($site_dir != '') { echo '/'.$site_dir.'/';}  ?>news.php"><i class="fad fa-newspaper"></i> News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php if($site_dir == '') { echo '/';} elseif ($site_dir != '') { echo '/'.$site_dir.'/';}  ?>pvp.php"><i class="fad fa-swords"></i> PvP</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php if($site_dir == '') { echo '/';} elseif ($site_dir != '') { echo '/'.$site_dir.'/';}  ?>shop.php"><i class="fad fa-store"></i> Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php if($site_dir == '') { echo '/';} elseif ($site_dir != '') { echo '/'.$site_dir.'/';}  ?>help.php"><i class="fad fa-life-ring"></i> Help</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fad fa-user-circle"></i> Account
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<?php 
					if(isset($_SESSION['id']))
					{
						echo '
						<a class="dropdown-item" href="'.$custdir.'/ucp.php"><i class="fad fa-user-cog"></i> UCP</a>
						<a class="dropdown-item" href="'.$custdir.'/buy-coins.php"><i class="fad fa-coin"></i> Buy Coins</a>
						<a class="dropdown-item" href="'.$custdir.'/logout.php"><i class="fad fa-sign-out"></i> Logout</a>
						';
					} 
					else
					{
						echo '
                        <a class="dropdown-item" href="'.$custdir.'/login.php"><i class="fad fa-sign-in"></i> Login</a>
                        <a class="dropdown-item" href="'.$custdir.'/register.php"><i class="fad fa-user-plus"></i> Register</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="'.$custdir.'/recover.php"><i class="fad fa-unlock-alt"></i> Recover password</a>
						';
					}
					?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
