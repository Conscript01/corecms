<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-users-cog"></i> View accounts</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-dark" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Header</th>
                            <th>News</th>
                            <th>Author</th>
                            <th>Created At</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Header</th>
                            <th>News</th>
                            <th>Author</th>
                            <th>Created At</th>
                            <th>Tools</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        if($num_news < 1)
                        {
                            echo '<tr><td colspan="6">There are no news!</td></tr>';
                        }
                        else
                        {
                            while($res = $news_query->fetch_assoc())
                            {
                                $ID = $res['id'];
                                $header = $res['header'];
                                $news = $res['news'];
                                $author = $res['author'];
                                $created_at = $res['created_at'];
                                echo '
                                    <tr>
                                       <td>'. $ID .'</td>
                                       <td>'. $header .'</td>
                                       <td>'. $news .'</td>
                                       <td>'. $author .'</td>
                                       <td>'. $created_at .'</td>
                                       <td><a href="'.$custdir.'/acp/news-delete.php?id='. $ID .'" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure?\')"><i class="fad fa-trash"></i> Delete</a></td></td>
                                    </tr>
                                    ';
                            }
                        }

                        ?>

                        </tbody>
                    </table>
                    <a href="<?php echo $custdir; ?>/acp/news-add.php" class="btn btn-success"><i class="fad fa-plus-circle"></i> Add news</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>