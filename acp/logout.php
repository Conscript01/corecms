<?php
include('../config.php');
if($site_dir == '') {
    $custdir = '';
} elseif ($site_dir != '') 
{ 
    $custdir = '/'.$site_dir;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Manager - SC Autogas Impex SRL">
    <meta name="author" content="Laboratory 404">

    <title>CoreCms Manager Logout</title>

    <!-- Bootstrap core CSS-->
    <link href="<?php echo $custdir; ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="<?php echo $custdir; ?>/assets/fontawesome/css/all.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
            <?php
            if(isset($_SESSION['acp']))
            {
               session_destroy();
                header("Location: $custdir/index.php");
            }
            else
            {
                header("Location: $custdir/index.php");
            }
            ?>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo $custdir; ?>/assets/jquery/jquery.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo $custdir; ?>/assets/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
